import java.util.HashMap;

import gitlabAPI.CreateMethods;
import gitlabAPI.GetMethods;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;



import static spark.Spark.*;

public class Main {
	public static void main(String[] args) {
		staticFileLocation("/public");
		String layout = "templates/layout.vtl";

		int timeOutMillis = 999999;
		threadPool(timeOutMillis);

		// Generate HomePage - redirect to config page if this is the first visit.
		get("/", (req, res) -> {
			HashMap model = new HashMap();

			req.session(true);
			String a = req.session().attribute("hostUrl");
			String b = req.session().attribute("apiToken");
			if (a == null || b == null || a == "" || b == "") {

				req.session().attribute("hostUrl", "");
				req.session().attribute("apiToken", "");
				req.session().attribute("description", "");
				req.session().attribute("issuesEnabled", "");
				req.session().attribute("wallEnabled", "");
				req.session().attribute("mergeRequestsEnabled", "");
				req.session().attribute("wikiEnabled", "");
				req.session().attribute("snippetsEnabled", "");
				req.session().attribute("publik", "");
				req.session().attribute("visibilityLevel", "");
				req.session().attribute("connected", "Fill In Settings.");

				res.redirect("/config");
			} else {
				System.out.println("Host: " + a);
				System.out.println("API: " + b);

				GetMethods got = new GetMethods();
				String connect = got.connect(a, b);
				// Check connection status
				if (connect == "Not Connected - Incorrect URL/Api Token"){
					model.put("connected", connect); 
				} else {
					model.put("connected", connect);
					String modules = got.getAllModules();
					model.put("modules", modules);
				}

				model.put("template", "templates/home.vtl");
				return new ModelAndView(model, layout);
			}

			return new ModelAndView(model, layout);
		}, new VelocityTemplateEngine());


		// Generate HomePage
		get("/module", (req, res) -> {
			HashMap model = new HashMap();
			model.put("connected", "Connected");
			String module = req.queryParams("module");

			req.session(true);
			String a = req.session().attribute("hostUrl");
			String b = req.session().attribute("apiToken");

			GetMethods got = new GetMethods();
			got.connect(a, b);

			String projects = got.getIssues(module);
			String[] commits = got.getAllProjectData(module);

			model.put("module", module);  
			model.put("projects", projects);
			model.put("builds", commits[1]);
			model.put("commits", commits[0]);
			model.put("countprojects", commits[2]);
			model.put("countbuilds", commits[3]);
			model.put("updatedlastweek", commits[4]);
			model.put("notupdated", commits[5]);
			model.put("template", "templates/module.vtl");
			return new ModelAndView(model, layout);
		}, new VelocityTemplateEngine());


		// Generate Config Page
		get("/config", (req, res) -> {

			HashMap model = new HashMap();

			String a = req.session().attribute("hostUrl");
			String b = req.session().attribute("apiToken");

			// Add Variables
			model.put("hostUrl", req.session().attribute("hostUrl"));
			model.put("apiToken", req.session().attribute("apiToken"));
			model.put("description", req.session().attribute("description"));
			model.put("issuesEnabled", req.session().attribute("issuesEnabled"));
			model.put("wallEnabled", req.session().attribute("wallEnabled"));
			model.put("mergeRequestsEnabled", req.session().attribute("mergeRequestsEnabled"));
			model.put("wikiEnabled", req.session().attribute("wikiEnabled"));
			model.put("snippetsEnabled", req.session().attribute("snippetsEnabled"));
			model.put("publik", req.session().attribute("publik"));
			model.put("visibilityLevel", req.session().attribute("visibilityLevel"));

			GetMethods got = new GetMethods();
			String connect = got.connect(a, b);

			// Check connection status
			if (connect == "Not Connected - Incorrect URL/Api Token"){
				model.put("connected", connect); 
			} else {
				model.put("connected", connect);
			}     	

			model.put("template", "templates/config.vtl");

			return new ModelAndView(model, layout);
		}, new VelocityTemplateEngine());


		// Update Config
		get("/updateconfig", (req, res) -> {


			//Fetch Results
			String hostUrl = req.queryParams("hostUrl");
			String apiToken = req.queryParams("apiToken");
			String description = req.queryParams("description");
			String issuesEnabled = req.queryParams("issuesEnabled");
			String wallEnabled = req.queryParams("wallEnabled");
			String mergeRequestsEnabled = req.queryParams("mergeRequestsEnabled");
			String wikiEnabled = req.queryParams("wikiEnabled");
			String snippetsEnabled = req.queryParams("snippetsEnabled");
			String publik = req.queryParams("publik");
			String visibilityLevel = req.queryParams("visibilityLevel");

			req.session(true);
			req.session().attribute("hostUrl", hostUrl);
			req.session().attribute("apiToken", apiToken);
			req.session().attribute("description", description);
			req.session().attribute("issuesEnabled", issuesEnabled);
			req.session().attribute("wallEnabled", wallEnabled);
			req.session().attribute("mergeRequestsEnabled", mergeRequestsEnabled);
			req.session().attribute("wikiEnabled", wikiEnabled);
			req.session().attribute("snippetsEnabled", snippetsEnabled);
			req.session().attribute("publik", publik);
			req.session().attribute("visibilityLevel", visibilityLevel);

			HashMap model = new HashMap();
			res.redirect("/config");

			model.put("template", "templates/config.vtl");
			return new ModelAndView(model, layout);
		}, new VelocityTemplateEngine());


		// Add Module
		get("/addmodule", (req, res) -> {
			HashMap model = new HashMap();

			req.session(true);
			String a = req.session().attribute("hostUrl");
			String b = req.session().attribute("apiToken");

			GetMethods got = new GetMethods();
			String connect = got.connect(a, b);
			// Check connection status
			if (connect == "Not Connected - Incorrect URL/Api Token"){
				model.put("connected", connect); 
			} else {
				model.put("connected", connect);
				String imports = got.getImports(); 

				model.put("imports", imports);
				model.put("template", "templates/addmodule.vtl");
			}

			return new ModelAndView(model, layout);
		}, new VelocityTemplateEngine());


		// Create Module
		get("/createmodule", (req, res) -> {

			String moduleName = req.queryParams("moduleName");
			String content = req.queryParams("fileContent");
			String importUrl = req.queryParams("import");
			String result = "";

			req.session(true);
			String a = req.session().attribute("hostUrl");
			String b = req.session().attribute("apiToken");
			String description = req.session().attribute("description");
			String issuesEnabled = req.session().attribute("issuesEnabled");
			String wallEnabled = req.session().attribute("wallEnabled");
			String mergeRequestsEnabled = req.session().attribute("mergeRequestsEnabled");
			String wikiEnabled = req.session().attribute("wikiEnabled");
			String snippetsEnabled = req.session().attribute("snippetsEnabled");
			String publik = req.session().attribute("publik");

			// setup
			CreateMethods create = new CreateMethods();
			create.connect(a, b);

			// Create Group
			try {
				result += create.addModule(moduleName);
				result += "<br><br>";
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Split users into array
			String[] Users = content.split("\\s*,\\s*");

			// Create Loop
			for (int x = 0; x < Users.length; x++){

				// Create User
				if (a.contains("gitlab.com")) {
					result += "<tr><td>" + x + "</td>";
					result += "<td> Unable to create user: " + Users[x] + " - Access denied</td>";
				} else {
					result += "<tr><td>" + x + "</td>";
					result += "<td>" + create.addUser(Users[x]) + "</td>";
				}
				// Create Project
				result += "<td>" + create.addProject(moduleName, Users[x], importUrl, description, issuesEnabled, wallEnabled, mergeRequestsEnabled, wikiEnabled, snippetsEnabled, publik) + "</td>";

				// Add User to Project
				result += "<td>" + create.addUserToProject(moduleName, Users[x]) + "</td>";
				result += "</tr>";

			}

			HashMap model = new HashMap();
			model.put("connected", "Connected");
			model.put("result", result);
			model.put("template", "templates/addmoduleresult.vtl");
			return new ModelAndView(model, layout);
		}, new VelocityTemplateEngine());


		// Export All Data
		get("/export", (req, res) -> {
			HashMap model = new HashMap();

			model.put("connected", "Connected");

			req.session(true);
			String a = req.session().attribute("hostUrl");
			String b = req.session().attribute("apiToken");

			GetMethods got = new GetMethods();
			got.connect(a, b);
			String export = got.getExport(); 

			model.put("export", export);
			model.put("template", "templates/export.vtl");
			return new ModelAndView(model, layout);
		}, new VelocityTemplateEngine());


		// Export Module data
		get("/exportmodule", (req, res) -> {
			HashMap model = new HashMap();

			model.put("connected", "Connected");

			req.session(true);
			String a = req.session().attribute("hostUrl");
			String b = req.session().attribute("apiToken");

			int moduleId = Integer.parseInt(req.queryParams("moduleId"));

			GetMethods got = new GetMethods();
			got.connect(a, b);
			String export = got.getModuleExport(moduleId); 

			model.put("export", export);
			model.put("template", "templates/export.vtl");
			return new ModelAndView(model, layout);
		}, new VelocityTemplateEngine());


		// Help Page
		get("/help", (req, res) -> {
			HashMap model = new HashMap();
			model.put("connected", "");

			req.session(true);
			String a = req.session().attribute("hostUrl");
			String b = req.session().attribute("apiToken");

			model.put("template", "templates/help.vtl");
			return new ModelAndView(model, layout);
		}, new VelocityTemplateEngine());


		// close server
		get("/stop", (req, res) -> {
			HashMap model = new HashMap();

			stop();

			model.put("template", "templates/home.vtl");
			return new ModelAndView(model, layout);
		}, new VelocityTemplateEngine());

	}
}