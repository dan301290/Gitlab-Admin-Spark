package gitlabAPI;

import java.util.Date;

import org.gitlab.api.*;
import org.gitlab.api.http.*;
import org.gitlab.api.models.*;

public class GetMethods {

	// Setup
	static String hosturl;
	static String apitoken;
	static GitlabProject projectID = null;
	static GitlabUser userID = null;
	static GitlabAccessLevel accessLevel =  GitlabAccessLevel.Developer;
	static GitlabAPI api = null;
	static GitlabSession ses = null;
	
	// Connect - test if API Token/Host URL correspond to legitimate GitLab instance
	public String connect(String HostUrl, String ApiToken) {
		try {
			api = GitlabAPI.connect(HostUrl, ApiToken);
			api.getCurrentSession();
			return "Connected";
		}
		catch (Exception e){
			return "Not Connected - Incorrect URL/Api Token";
		}
	}

	
	// Get All Modules (Groups) from Gitlab.	
	public String getAllModules () {
		String result = "";
		try {
			GitlabGroup[] groupArray = api.getGroups().toArray(new GitlabGroup[api.getGroups().size()]);
			for (GitlabGroup group : groupArray) {
				result += "<tr>";
				result += "<td>" + "<a href='/module?module=" + group.getPath() + "' >" + group.getName() + "</a>" + "</td>";
				result += "<td>" + "<a href='/exportmodule?moduleId=" + group.getId() + "' >Export</a>" + "</td>";
				result += "</tr>";
			}
			return result;
		} catch (Exception e){
			return "Failed to get modules";
		}
	}

	
	// Get All Issues from Gitlab.
	public String getIssues (String module) {
		String result = "";
		try {
			GitlabGroup[] groupArray = api.getGroups().toArray(new GitlabGroup[api.getGroups().size()]);
			for (GitlabGroup group : groupArray){
				if (group.getName().matches(module)){
					GitlabProject[] projectArray = api.getGroupProjects(group).toArray(new GitlabProject[api.getGroupProjects(group).size()]);
					for (GitlabProject project : projectArray) {

						GitlabIssue[] issueArray = api.getIssues(project).toArray(new GitlabIssue[api.getIssues(project).size()]);
						for (GitlabIssue issue : issueArray) {

							String url = project.getHttpUrl().replaceAll(".git", "/issues/").concat(Integer.toString(issue.getIid()));
							result += "<tr>";
							result += "<td><a href='" + url + "' target='_blank'>" + project.getNameWithNamespace() + "</a></td>";
							result += "<td>" + issue.getTitle() + "<br>" + "</td>";
							result += "<td>" + issue.getDescription() + "</td>";
							result += "<td>" + issue.getState() + "</td>";
							result += "<td>" + issue.getUpdatedAt() + "</td>";
							result += "</tr>";
						}
					}
				}
			} return result;
		} catch (Exception e) {
			return "Failed to get issues";
		}
	}

	
	// Get All Projects/project info from Gitlab.
	public String[] getAllProjectData (String module) {
		String result = "";
		String builds = "";
		int countProjects = 0;
		int countBuilds = 0;
		int updatedlastweek = 0;
		int notupdated = 0;

		try {
			GitlabGroup[] groupArray = api.getGroups().toArray(new GitlabGroup[api.getGroups().size()]);
			for (GitlabGroup group : groupArray){
				if (group.getName().matches(module)){
					GitlabProject[] projectArray = api.getGroupProjects(group).toArray(new GitlabProject[api.getGroupProjects(group).size()]);

					for (GitlabProject project : projectArray) {

						GitlabBranch branch = api.getBranch(project, "master");
						GitlabBranchCommit a = branch.getCommit();

						result += "<tr>";
						result += "<td><a href='" + project.getHttpUrl() + "' target='_blank'>" + project.getName() + "</a></td>";
						result += "<td>" + a.getMessage() + "</td>";
						result += "<td>" + a.getCommittedDate() + "</td>";
						result += "</tr>";
						countProjects++;

						Date date = new Date();
						if (a.getCommittedDate().getTime() > date.getTime()-604800000) {
							updatedlastweek++;
						} else {
							notupdated++;
						}

						GitlabBuild[] buildsArray = api.getProjectBuilds(project).toArray(new GitlabBuild[api.getProjectBuilds(project).size()]);
						for (GitlabBuild build : buildsArray) {
							builds += "<tr>";
							builds += "<td><a target='_blank' href='" + project.getHttpUrl().replace(".git", "/commit/") + build.getCommit().getId() + "/builds'>"+ project.getName() + "</a></td>";
							builds += "<td>" + build.getName() + "</td>";
							builds += "<td>" + build.getStatus() + "</td>";
							builds += "</tr>";
							countBuilds++;
						}

					}

				} 
			} 

			String[] x = {result,builds,Integer.toString(countProjects),Integer.toString(countBuilds),Integer.toString(updatedlastweek),Integer.toString(notupdated)};
			System.out.println(x);
			return x;
		} catch(Exception e) {
			e.printStackTrace();
			String[] x = {"Failed","Failed","Failed","Failed","Failed","Failed"};
			return x;
		}
	}

	
	// Get Admins projects, to use as an import.	
	public String getImports (){
		String imports = "";
		try {
			GitlabUser user = api.getUser();
			GitlabProject[] projectArray = api.getProjectsViaSudo(user).toArray(new GitlabProject[api.getProjectsViaSudo(user).size()]);

			for (GitlabProject project : projectArray) {
				if (project.getNamespace().getName().matches(user.getUsername())){
					imports += "<option>" + project.getHttpUrl() + "</option>";
				}
			}

			return imports;
		} catch (Exception e) {
			return "failed to get admin projects";
		}
	}

	
	// Get All Info from Gitlab and perpare Export.
	public String getExport () {
		String export = "<?xml version='1.0' encoding='UTF-8'?>" + System.lineSeparator();
		export += "<modules>" + System.lineSeparator();
		try {

			GitlabGroup[] groupArray = api.getGroups().toArray(new GitlabGroup[api.getGroups().size()]);
			for (GitlabGroup group : groupArray) {
				export += "	<module>" + System.lineSeparator();
				export += "		<ModuleName>" + group.getName() + "</ModuleName>" + System.lineSeparator();

				GitlabProject[] projectArray = api.getGroupProjects(group).toArray(new GitlabProject[api.getGroupProjects(group).size()]);
				for (GitlabProject project : projectArray) {
					export += "		<project>" + System.lineSeparator();

					export += "			<ProjectName>" + project.getName() + "</ProjectName>" + System.lineSeparator();
					export += "			<ProjectDesc>" + project.getDescription() + "</ProjectDesc>" + System.lineSeparator();
					export += "			<ProjectURL>" + project.getHttpUrl() + "</ProjectURL>" + System.lineSeparator();
					// Get latest commit
					GitlabBranch branch = api.getBranch(project, "master");
					GitlabBranchCommit latestcommit = branch.getCommit();
					export += "			<LatestCommitMessage>" + latestcommit.getMessage() + "</LatestCommitMessage>" + System.lineSeparator();
					export += "			<LastUpdated>" + project.getLastActivityAt() + "</LastUpdated>" + System.lineSeparator();
					export += "			<CreatedDate>" + project.getCreatedAt() + "</CreatedDate>" + System.lineSeparator();

					// Get All Commits
					GitlabCommit[] commitsArray = api.getAllCommits(project.getId(),"master").toArray(new GitlabCommit[api.getAllCommits(project.getId(),"master").size()]);
					export += "			<commits>" + System.lineSeparator();
					for (GitlabCommit commit : commitsArray) {
						export += "				<commit>" + System.lineSeparator();
						export += "					<CommitTitle>" + commit.getTitle() + "</CommitTitle>" + System.lineSeparator();
						export += "					<CommitDesc>" + commit.getDescription() + "</CommitDesc>" + System.lineSeparator();
						export += "					<CommitCreatedDate>" + commit.getCreatedAt() + "</CommitCreatedDate>" + System.lineSeparator();
						export += "				<commit>" + System.lineSeparator();
					}
					export += "			</commits>" + System.lineSeparator();

					// Get All Builds
					GitlabBuild[] buildsArray = api.getProjectBuilds(project).toArray(new GitlabBuild[api.getProjectBuilds(project).size()]);
					export += "			<builds>" + System.lineSeparator();
					for (GitlabBuild build : buildsArray) {
						export += "				<build>" + System.lineSeparator();
						export += "					<BuildTask>" + build.getName() + "</BuildTask>" + System.lineSeparator();
						export += "					<BuildStatus>" + build.getStatus() + "</BuildStatus>" + System.lineSeparator();
						export += "					<BuildDate>" + build.getCreatedAt() + "</BuildDate>" + System.lineSeparator();
						export += "				<build>" + System.lineSeparator();
					}
					export += "			</builds>" + System.lineSeparator();

					// Get All Issues
					GitlabIssue[] issueArray = api.getIssues(project).toArray(new GitlabIssue[api.getIssues(project).size()]);
					export += "			<issues>" + System.lineSeparator();
					for (GitlabIssue issue : issueArray) {
						export += "				<issue>" + System.lineSeparator();
						export += "					<IssueTitle>" + issue.getTitle() + "</IssueTitle>" + System.lineSeparator();
						export += "					<IssueDesc>" + issue.getDescription() + "</IssueDesc>" + System.lineSeparator();
						export += "					<IssueStatus>" + issue.getState() + "</IssueStatus>" + System.lineSeparator();
						export += "					<IssueUpdatedAt>" + issue.getUpdatedAt() + "</IssueUpdatedAt>" + System.lineSeparator();
						export += "				<issue>" + System.lineSeparator();
					}
					export += "			<issues>" + System.lineSeparator();

					export += "		</project>" + System.lineSeparator();
				}

				export += "	</module>" + System.lineSeparator();
			}
			export += "<modules>" + System.lineSeparator();
			System.out.println(export);
			return export;
		} catch (Exception e) {
			e.printStackTrace();
			return "Failed to get info - Projects exist that have not been fully setup.";
		}
	}

	
	// Get All Info for a Gitlab Module and prepare Export.
	public String getModuleExport (int moduleId) {
		String export = "<?xml version='1.0' encoding='UTF-8'?>" + System.lineSeparator();
		export += "<modules>" + System.lineSeparator();
		try {

			//GitlabGroup[] groupArray = api.getGroups().toArray(new GitlabGroup[api.getGroups().size()]);
			GitlabGroup group = api.getGroup(moduleId);
			System.out.println("Module: " + moduleId);

			export += "	<module>" + System.lineSeparator();
			export += "		<ModuleName>" + group.getName() + "</ModuleName>" + System.lineSeparator();

			GitlabProject[] projectArray = api.getGroupProjects(group).toArray(new GitlabProject[api.getGroupProjects(group).size()]);
			for (GitlabProject project : projectArray) {
				export += "		<project>" + System.lineSeparator();

				export += "			<ProjectName>" + project.getName() + "</ProjectName>" + System.lineSeparator();
				export += "			<ProjectDesc>" + project.getDescription() + "</ProjectDesc>" + System.lineSeparator();
				export += "			<ProjectURL>" + project.getHttpUrl() + "</ProjectURL>" + System.lineSeparator();
				// Get latest commit
				GitlabBranch branch = api.getBranch(project, "master");
				GitlabBranchCommit latestcommit = branch.getCommit();
				export += "			<LatestCommitMessage>" + latestcommit.getMessage() + "</LatestCommitMessage>" + System.lineSeparator();
				export += "			<LastUpdated>" + project.getLastActivityAt() + "</LastUpdated>" + System.lineSeparator();
				export += "			<CreatedDate>" + project.getCreatedAt() + "</CreatedDate>" + System.lineSeparator();

				// Get All Commits
				GitlabCommit[] commitsArray = api.getAllCommits(project.getId(),"master").toArray(new GitlabCommit[api.getAllCommits(project.getId(),"master").size()]);
				export += "			<commits>" + System.lineSeparator();
				for (GitlabCommit commit : commitsArray) {
					export += "				<commit>" + System.lineSeparator();
					export += "					<CommitTitle>" + commit.getTitle() + "</CommitTitle>" + System.lineSeparator();
					export += "					<CommitDesc>" + commit.getDescription() + "</CommitDesc>" + System.lineSeparator();
					export += "					<CommitCreatedDate>" + commit.getCreatedAt() + "</CommitCreatedDate>" + System.lineSeparator();
					export += "				<commit>" + System.lineSeparator();
				}
				export += "			</commits>" + System.lineSeparator();

				// Get All Builds
				GitlabBuild[] buildsArray = api.getProjectBuilds(project).toArray(new GitlabBuild[api.getProjectBuilds(project).size()]);
				export += "			<builds>" + System.lineSeparator();
				for (GitlabBuild build : buildsArray) {
					export += "				<build>" + System.lineSeparator();
					export += "					<BuildTask>" + build.getName() + "</BuildTask>" + System.lineSeparator();
					export += "					<BuildStatus>" + build.getStatus() + "</BuildStatus>" + System.lineSeparator();
					export += "					<BuildDate>" + build.getCreatedAt() + "</BuildDate>" + System.lineSeparator();
					export += "				<build>" + System.lineSeparator();
				}
				export += "			</builds>" + System.lineSeparator();

				// Get All Issues
				GitlabIssue[] issueArray = api.getIssues(project).toArray(new GitlabIssue[api.getIssues(project).size()]);
				export += "			<issues>" + System.lineSeparator();
				for (GitlabIssue issue : issueArray) {
					export += "				<issue>" + System.lineSeparator();
					export += "					<IssueTitle>" + issue.getTitle() + "</IssueTitle>" + System.lineSeparator();
					export += "					<IssueDesc>" + issue.getDescription() + "</IssueDesc>" + System.lineSeparator();
					export += "					<IssueStatus>" + issue.getState() + "</IssueStatus>" + System.lineSeparator();
					export += "					<IssueUpdatedAt>" + issue.getUpdatedAt() + "</IssueUpdatedAt>" + System.lineSeparator();
					export += "				<issue>" + System.lineSeparator();
				}
				export += "			<issues>" + System.lineSeparator();

				export += "		</project>" + System.lineSeparator();
			}

			export += "	</module>" + System.lineSeparator();


			export += "<modules>" + System.lineSeparator();
			System.out.println(export);
			return export;
		} catch (Exception e) {
			return "Failed to get info - Projects exist that have not been fully setup.";
		}
	}
}
