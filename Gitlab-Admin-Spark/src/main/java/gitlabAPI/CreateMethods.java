package gitlabAPI;

import java.io.IOException;

import org.gitlab.api.*;
import org.gitlab.api.http.*;
import org.gitlab.api.models.*;

public class CreateMethods {
	// Setup
	static GitlabAPI api = null;
	static GitlabAccessLevel accessLevel =  GitlabAccessLevel.Developer;
	static GitlabProject projectID = null;
	static GitlabUser userID = null;
	static GitlabUser Admin = null;

	// User settings
	static String email,username = null;
	static String bio,extern_provider_name,extern_uid,twitter,skypeId,password,linkedIn,website_url,fullName = "null";
	static Integer projects_limit = 20;
	static Boolean skip_confirmation = true;
	static Boolean can_create_group = false;
	static Boolean isAdmin = false;

	public void connect(String HostUrl, String ApiToken) {

		api = GitlabAPI.connect(HostUrl, ApiToken);

		System.out.println(HostUrl + " : " + ApiToken);
	}

	public String addUser(String userName){

		// Create User
		try {
			username = userName.replace("@leeds.ac.uk", "");
			extern_uid = userName.replace("@leeds.ac.uk", "");
			extern_provider_name = userName.replace("@leeds.ac.uk", "");
			fullName = userName.replace("@leeds.ac.uk", "");
			email = userName;
			password = userName.replace("@leeds.ac.uk", "")+"pass";

			api.createUser(email, password, username, fullName, skypeId, linkedIn, twitter, website_url, projects_limit, extern_uid, extern_provider_name, bio, isAdmin, can_create_group, skip_confirmation);
			return userName + ": created";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return userName + ": User already exists.";
		}
	}

	public String addModule(String moduleCode) throws Exception{
		try{
			api.createGroup(moduleCode, moduleCode);
			return "Created Module: " + moduleCode;
		}catch(Exception e){
			//e.printStackTrace();
			return "Failed to add Module: " + moduleCode + " - Already exists";
		}
	}

	public String addProject(String projectName,String userName,String importUrl,String description,String issuesEnabled,String wallEnabled,String mergeRequestsEnabled,String wikiEnabled,String snippetsEnabled,String publik){

		userName = userName.replace("@leeds.ac.uk", "").replace("@gmail.com", "");
		
		// Create Project
		try{
			GitlabGroup[] array = api.getGroups().toArray(new GitlabGroup[api.getGroups().size()]);
			int namespaceId = 1;
			for (GitlabGroup value : array){
				if (value.getName().matches(projectName)){
					namespaceId = value.getId();
				}
			}
			api.createProject(userName.replace("@leeds.ac.uk", "").replace("@gmail.com", ""), namespaceId, description, Boolean.valueOf(issuesEnabled), Boolean.valueOf(wallEnabled), Boolean.valueOf(mergeRequestsEnabled), Boolean.valueOf(wikiEnabled), Boolean.valueOf(snippetsEnabled), Boolean.valueOf(publik), 0, importUrl);
			
			return "Added Project: " + userName + " to Module: " + projectName;
		}catch(Exception e) {
			e.printStackTrace();
			return "Failed to add Project: " + userName + " to Module: " + projectName + " - Already exists";
		}
	}

	public String addUserToProject(String projectName,String userName){

		userName = userName.replace("@leeds.ac.uk", "").replace("@gmail.com", "");

		try{
			Admin = api.getUser();
			GitlabUser[] userArray = api.findUsers(userName).toArray(new GitlabUser[api.findUsers(userName).size()]);
			GitlabProject[] projectArray = api.getProjectsViaSudo(Admin).toArray(new GitlabProject[api.getProjectsViaSudo(Admin).size()]);


			for (GitlabUser valueU : userArray){
				if (valueU.getName().matches(userName)){
					userID = valueU;
				}
			}


			for (GitlabProject valueP : projectArray){
				if (valueP.getName().matches(userID.getUsername())){
					projectID = valueP;
				}
			}

			api.addProjectMember(projectID, userID, accessLevel);
			return "Added User: " + userName + " to project: " + userName;

		}catch(Exception e) {
			e.printStackTrace();
			return "Failed to add User: " + userName + " to project: " + userName;
		}
	}

}
