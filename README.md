# Repository for GitLab-Admin

## About
This program provides a custom interface for interacting with a GitLab instance (The Host URL and Api Token of this instance are needed) that adapts Gitlab into a coursework submission system for software related projects. The software has been developed using the Java Spark framework (http://sparkjava.com/).

## Aim
The aim of this project is to adapt Gitlab into a coursework submission system with the following benefits:
1. Students gain exposure to version control and repository hosting systems that they will inevitably have to use in their careers.
2. Lecturers/teachers get useful data showing how their students go about completing their coursework assignments i.e. 
2.1 The average number of commits a student makes, does the average student make regular commits or rush to complete the work just before the deadline.
2.2 Highlights students that have not made any commits as the deadline approaches, the lecturer can contact these students to discuss if they are having problems.
2.3 A student can raise issues and the lecturer is able to see the source code within the repository and provide guidance and feedback throughout the coursework duration - specifically without the student needing to arrange an appointment and bring their laptop/source code with them.
3. Make use of Gitlab's Continuous Integration feature to check if a student’s code compiles and produces the expected output, while the students source code would still need to be manually checked, the automation of compilation and output tests would speed up the process of assessing students work.


## Features
The features included are as follows:

1. The ability to set up a module/class on a GitLab instance via a CSV file, this process will create a user account and repository for each student. - The repository is cloned from a repository created by the teacher/lecturer and should include the basic structure file structure of the coursework, a README.md file outlining the instructions for the coursework and a "gitlab-ci.yml" file that details the compilation and test instructions for the program (documentation can be found here https://docs.gitlab.com/ee/ci/yaml/)
2. The ability to export module data from a GitLab instance (Number of commits, dates, issues etc.). for further analysis.
3. The ability to tests a student’s code upon each commit by making use of GitLab’s built in Continuous Integration feature. - The code is compiled and ran, with its output being checked to ensure it matches what is expected.
4. The ability to see a module overview for a GitLab instance, including latest activity per project, CI (Continuous Integration) status per project, the issue tracker and overall data showing the number of students that have submitted code that has succeeded in passing the CI checks and highlighting students that have yet to make a commit.

## Setup
A complied version of the program is included in this repository and can be found at https://gitlab.com/Gitlab-Coursework-Submission-Project/Gitlab-Admin-Spark/blob/master/Gitlab-Admin-Spark/target/Gitlab-Admin-Spark-1.0.jar
 
To use this program, do the following:

1. Ensure Java 1.8 is installed on your machine.
2. Download and run Gitlab-Admin-Spark-1.0.jar
3. Point your browser to the following URL http://localhost:4567

Your own host URL and Api Token can be used but for testing purposes
Directing the browser to http://localhost:4567/stop will shut down the server.
